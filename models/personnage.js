import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const personnageSchema = new Schema ({
	nom: {
		type: String,
		required: true,
		unique: true
	},
	niveau: {
		type: Number,
		required: true
	}
}, {collection: "personnage", timestamps: true});

export default mongoose.model('personnage',personnageSchema);