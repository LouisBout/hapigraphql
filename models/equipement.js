import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const equipementSchema = new Schema ({
	uid: {
		type: String,
		required: true
	},
	nom: {
		type: String,
		required: true,
		unique: true
	},
	niveau: {
		type: Number,
		required: true
	}
}, {collection: "equipement", timestamps: true});

export default mongoose.model('equipement',equipementSchema);