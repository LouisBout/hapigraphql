import mongoose from 'mongoose';
import hapi from 'hapi';
import { graphqlHapi, graphiqlHapi } from 'graphql-server-hapi';

import schema from './graphql';

const HOST = 'localhost';
const PORT = 3000;

const server = new hapi.Server();

const database = "test";
mongoose.connect(`mongodb://${HOST}:27017/${database}`);

const mongooseConnection = mongoose.connection;
mongooseConnection.on('error',() => console.log('Failed to connect'))
.once('open', () => console.log('Connected to DB'));




server.connection({
    host: HOST,
    port: PORT,
});

server.register({
    register: graphqlHapi,
    options: {
      path: '/graphql',
      graphqlOptions: {
        schema: schema
      },
      route: {
        cors: true
      }
    },
});

server.register({
  register: graphiqlHapi,
  options: {
    path: '/graphiql',
    graphiqlOptions: {
      endpointURL: '/graphql',
    },
  },
});

server.start((err) => {
    if (err) {
        throw err;
    }
    console.log(`Server running at: ${server.info.uri}`);
});