import Personnage from './single';
import Personnages from './multiple';
export default {
	Personnage,
	Personnages
};