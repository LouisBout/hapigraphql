import {
	GraphQLID,
	GraphQLNonNull
} from 'graphql';
import {personnageType} from '../../types/personnage';
import PersonnageModel from '../../../models/personnage';

export default {
	type: personnageType,
	args: {
		id: {
			name: 'id',
			type: new GraphQLNonNull(GraphQLID)
		}
	},
	resolve(root, params){
		return PersonnageModel.findById(params.id).exec();
	}
}