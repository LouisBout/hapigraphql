import {
	GraphQLList,
} from 'graphql';
import {personnageType} from '../../types/personnage';
import PersonnageModel from '../../../models/personnage';

export default {
	type: new GraphQLList(personnageType),
	resolve(){
		const personnages = PersonnageModel.find().exec();
		if(!personnages){
			throw new Error('Error while fetching personnages...');
		}
		return personnages;
	}
}