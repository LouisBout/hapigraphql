import {
	GraphQLNonNull
} from 'graphql';

import {personnageType, personnageInputType} from '../../types/personnage';
import PersonnageModel from '../../../models/personnage';

export default {
	type: personnageType,
	args: {
		data: {
			name: 'data',
			type: new GraphQLNonNull(personnageInputType)
		}
	},
	resolve(root,params){
		const pModel = new PersonnageModel(params.data);
		const newPersonnage = pModel.save();
		if(!newPersonnage){
			throw new Error('Error adding personnage');
		}
		return newPersonnage;
	}
};