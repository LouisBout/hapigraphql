import {
	GraphQLNonNull,
	GraphQLID
} from 'graphql';

import {personnageType, personnageInputType} from '../../types/personnage';
import PersonnageModel from '../../../models/personnage';

export default {
	type: personnageType,
	args: {
		id: {
			name: 'id',
			type: new GraphQLNonNull(GraphQLID)
		}
	},
	resolve(root,params){
		const removedPersonnage = PersonnageModel.findByIdAndRemove(params.id).exec();
		if(!removedPersonnage){
			throw new Error('Error removing personnage');
		}
		return removedPersonnage;
	}
};