import AddPersonnage from './add';
import RemovePersonnage from './remove';
import UpdatePersonnage from './update';

export default {
	AddPersonnage,
	RemovePersonnage,
	UpdatePersonnage
};