import {
	GraphQLNonNull,
	GraphQLID
} from 'graphql';

import {personnageType, personnageInputType} from '../../types/personnage';
import PersonnageModel from '../../../models/personnage';

export default {
	type: personnageType,
	args: {
		id: {
			name: 'id',
			type: new GraphQLNonNull(GraphQLID)
		},
		data: {
			name: 'data',
			type: new GraphQLNonNull(personnageInputType)
		}
	},
	resolve(root,params){
		return PersonnageModel.findByIdAndUpdate(params.id,{$set:{...params.data}})
		.then(data => PersonnageModel.findById(data.id).exec())
		.catch(err => new Error('could\'nt update personnage data ',err));
	}
};