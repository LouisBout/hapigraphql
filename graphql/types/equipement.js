import {
	GraphQLObjectType,
	GraphQLInputObjectType,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
	GraphQLList
} from 'graphql';

export const equipementType = new GraphQLObjectType({
	name: 'Equipement',
	description: 'Equipement',
	fields: () => ({
		_id: {
			type: new GraphQLNonNull(GraphQLID)
		},
		nom: {
			type: GraphQLString
		},
		niveau: {
			type: GraphQLInt
		}
	})
});

export const equipementInputType= new GraphQLInputObjectType({
	name: 'EquipementInput',
	description: 'Insert Equipement',
	fields:()=>({
		uid: {
			type: GraphQLString
		},
		nom: {
			type: GraphQLString
		},
		niveau: {
			type: GraphQLInt
		}
	})
});