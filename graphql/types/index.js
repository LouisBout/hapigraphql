import {personnageType, personnageInputType} from './personnage';
import {equipementType,equipementInputType} from './equipement';

export default {
	personnageType, personnageInputType,
	equipementType,equipementInputType
}