import {
	GraphQLObjectType,
	GraphQLInputObjectType,
	GraphQLNonNull,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
	GraphQLList
} from 'graphql';

import EquipementModel from '../../models/equipement';
import {equipementType} from './equipement';

export const personnageType = new GraphQLObjectType({
	name: 'Personnage',
	description: 'Personnage',
	fields: () => ({
		_id: {
			type: new GraphQLNonNull(GraphQLID)
		},
		nom: {
			type: GraphQLString
		},
		niveau: {
			type: GraphQLInt
		},
		equipements: {
			type: new GraphQLList(equipementType),
			resolve(personnage){
				const {_id} = personnage;
				return EquipementModel.find({uid: _id}).exec();

			}
		}
	})
});

export const personnageInputType=  new GraphQLInputObjectType({
	name: 'PersonnageInput',
	description: 'Insert Personnage',
	fields:()=>({
		nom: {
			type: GraphQLString
		},
		niveau: {
			type: GraphQLInt
		}
	})
});